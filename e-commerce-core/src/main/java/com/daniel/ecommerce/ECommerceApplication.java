package com.daniel.ecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.logging.Logger;

@SpringBootApplication
public class ECommerceApplication extends SpringBootServletInitializer {
	private static final Logger LOG = Logger.getLogger(ECommerceApplication.class.getName());

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(ECommerceApplication.class);
	}


	@Override
	public void onStartup(ServletContext servletContext) throws ServletException, ServletException {
		super.onStartup(servletContext);

	}

	public static void main(String[] args) {
		ConfigurableApplicationContext cntx = SpringApplication.run(ECommerceApplication.class, args);
		for (String name : cntx.getBeanDefinitionNames()) {
			LOG.info("Bean: "+name);
		}
	}
}
