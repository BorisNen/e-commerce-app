package com.daniel.ecommerce.controllers;

import java.util.*;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.daniel.ecommerce.payload.request.LoginRequest;
import com.daniel.ecommerce.payload.request.SignupRequest;
import com.daniel.ecommerce.payload.response.MessageResponse;
import com.daniel.ecommerce.repository.RoleRepository;
import com.daniel.ecommerce.security.jwt.JwtUtils;
import com.daniel.ecommerce.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.daniel.ecommerce.enums.ERole;
import com.daniel.ecommerce.entity.Roles;
import com.daniel.ecommerce.payload.response.JwtResponse;
import com.daniel.ecommerce.repository.UserRepository;
import com.daniel.ecommerce.security.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Value("${portal.app.cookie.bearer}")
    private String COOKIE_BEARER;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, HttpServletResponse response) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        userDetails.setPersonId(userDetails.getId());
        Cookie cookie = new Cookie(COOKIE_BEARER, jwt);
        // 7 days - 7 * 24 * 60 * 60
        cookie.setMaxAge(24 * 60 * 60);

        // Only set in production when connection is over SSL
//        cookie.setSecure(true);
        cookie.setHttpOnly(true);
        cookie.setPath("/");

        response.addCookie(cookie);

        return ResponseEntity.ok(new JwtResponse(userDetails.getUsername(), userDetails.getPersonId(), roles));
    }

    @GetMapping("/access")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> userAccess() {
        return ResponseEntity.ok(new MessageResponse("Достъпът разрешен!"));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Грешка: Потребителското име е заето!"));
        }

        // Create new user's account
        Users users = new Users(signUpRequest.getUsername(),
                encoder.encode(signUpRequest.getPassword()));

        Set<String> strRoles = signUpRequest.getRole();
        Set<Roles> roles = new HashSet<>();

        if (strRoles == null) {
            Roles userRoles = roleRepository.findByName(ERole.ROLE_USER.name())
                    .orElseThrow(() -> new RuntimeException("Error: Ролята не е открита."));
            roles.add(userRoles);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Roles adminRoles = roleRepository.findByName(ERole.ROLE_ADMIN.name())
                                .orElseThrow(() -> new RuntimeException("Error: Ролята не е открита."));
                        roles.add(adminRoles);
                        break;
                    default:
                        Roles userRoles = roleRepository.findByName(ERole.ROLE_USER.name())
                                .orElseThrow(() -> new RuntimeException("Error: Ролята не е открита."));
                        roles.add(userRoles);
                }
            });
        }

        users.setRoles(roles);
        userRepository.save(users);

        return ResponseEntity.ok(new MessageResponse("Потребителят е регистриран успешно!"));
    }

    @GetMapping("/users/search")
    @PreAuthorize("hasRole('ADMIN')")
    public List<Users> getUsers(@RequestParam(required = false) String username,
                                @RequestParam(required = false) String roles) {

        return new ArrayList<>(userRepository.findUsersByUsernameAndRolesName(username.toLowerCase(), roles));
    }
}
