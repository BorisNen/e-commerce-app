package com.daniel.ecommerce.payload.response;

import java.util.List;

public class JwtResponse {

    private String username;
    private Long personId;
    private List<String> roles;

    public JwtResponse(String username, List<String> roles) {
        this.username = username;
        this.roles = roles;
    }

    public JwtResponse(String username, Long personId, List<String> roles) {
        this.username = username;
        this.personId = personId;
        this.roles = roles;
    }

    public List<String> getRoles() {
        return roles;
    }

    public String getUsername() {
        return username;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
