package com.daniel.ecommerce.payload.response;

public class MessageResponse {
    private String message;
    private String name;
    private Long generatedId;

    public MessageResponse(String message) {
        this.message = message;
    }

    public MessageResponse(String message, Long generatedId) {
        this.message = message;
        this.generatedId = generatedId;
    }

    public MessageResponse(String message, String name, Long generatedId) {
        this.message = message;
        this.name = name;
        this.generatedId = generatedId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getGeneratedId() {
        return generatedId;
    }

    public void setGeneratedId(Long generatedId) {
        this.generatedId = generatedId;
    }
}
