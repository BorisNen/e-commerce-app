package com.daniel.ecommerce.enums;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
