package com.daniel.ecommerce.repository;

import java.util.List;
import java.util.Optional;

import com.daniel.ecommerce.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<Users, Long> {
    Optional<Users> findByUsername(String username);

    @Query("SELECT u " +
            "FROM Users u JOIN u.roles r " +
            "WHERE lower(u.username)" +
            "LIKE :#{#username == null || #username.isEmpty()? '%' : '%'+#username+'%'} " +
            "AND r.name LIKE :#{#name == null || #name.isEmpty()? '%' : #name}")
    List<Users> findUsersByUsernameAndRolesName(String username, String name);

    Boolean existsByUsername(String username);
}
