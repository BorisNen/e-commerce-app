export default class Person {
  constructor (firstName, middleName, lastName, personalNumber, age, telephone, address, city) {
    this.firstName = firstName
    this.middleName = middleName
    this.lastName = lastName
    this.personalNumber = personalNumber
    this.age = age
    this.telephone = telephone
    this.address = address
    this.city = city
  }
}
