import Vue from 'vue'
import * as axios from 'axios'
import App from './App.vue'
import { router } from './router'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import store from './store'
import Vuex from 'vuex'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'

axios.defaults.withCredentials = true

Vue.prototype.$axios = axios
// Vue.prototype.cookies = cookies
Vue.config.productionTip = false

// Bootstrap
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(Vuex)

axios.interceptors.response.use((response) => Promise.resolve(response),
  (error) => {
    if (error.response.status === 401) {
      store.dispatch('auth/logout')
      router.replace('login')
      return Promise.reject(error)
    }
    return Promise.reject(error.response)
  })

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
